/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot.chassis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tahomarobotics.robot.chassis.Chassis.Gear;

import edu.wpi.first.wpilibj.command.Command;

/**
 * Command to shift the drive transmission from low to high or high to low.
 *
 */
public class ShiftCommand extends Command {

	private static final Logger LOGGER = LoggerFactory.getLogger(ShiftCommand.class);
	
	// sub-system to apply the shift command
	private final Chassis chassis = Chassis.getInstance();

	// gear state to command to, null means toggle gear state
	private final Gear gear;
	
	/**
	 * Toggle version of the Drive Shift command.  Running this command
	 * is switch the transmission to the opposite state.
	 */
	public ShiftCommand() {
		this(null);
	}

	/**
	 * Shift gear to the specified state.
	 * 
	 * @param gear - LOW or HIGH
	 */
	public ShiftCommand(Gear gear) {
		this.gear = gear;
	}
	
	/**
	 * Command the chassis transmission to the specified gear or toggle it depending on 
	 * what command was constructed.
	 */
	@Override
	protected boolean isFinished() {
		
		chassis.setGear(gear != null ? gear : (chassis.getGearState() == Gear.LOW ? Gear.HIGH : Gear.LOW));
		
		LOGGER.info("shifted to " + chassis.getGearState());

		return true;
	}	
}
