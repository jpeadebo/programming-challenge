/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot.chassis;

import org.tahomarobotics.robot.OI;

import edu.wpi.first.wpilibj.command.Command;

public class TeleopDriveCommand extends Command {

	// larger sensitivity for rotation
	private static final double ROTATIONAL_SENSITIVITY = 3;
	
	// normal sensitivity for forward
	private static final double FORWARD_SENSITIVITY = 2;
	
	// used to command drive motors
	private final Chassis chassis = Chassis.getInstance();
	
	// used for throttle input
	private final OI oi = OI.getInstance();

	/**
	 * Manual teleoperation drive command
	 */
	public TeleopDriveCommand() {

		// as this is the default command for Chassis, this is required
		requires(chassis);
	}
	
	@Override
	protected boolean isFinished() {
		
		// obtain throttle values from controller
		double leftThrottle = oi.getLeftThrottle();
		double rightThrottle = oi.getRightThrottle();
		
		// convert left and right controls to forward and rotate so that each can be 
		// desensitized with different values
		double forwardPower = desensitizePowerBased((rightThrottle + leftThrottle)/2, FORWARD_SENSITIVITY);
		double rotatePower = desensitizePowerBased((rightThrottle - leftThrottle)/2, ROTATIONAL_SENSITIVITY);
		
		chassis.setPower(forwardPower, rotatePower);
		
		return false;
	}

	/**
	 * Reduces the sensitivity around the zero point to make the Robot more 
	 * controllable.
	 * 
	 * @param value - raw input
	 * @param power - 1.0 indicates linear (full sensitivity) - larger number reduces small values
	 * 
	 * @return 0 to +/- 100%
	 */
	private double desensitizePowerBased(double value, double power) {
		return Math.abs(Math.pow(value, power - 1)) * value;
	}

}
